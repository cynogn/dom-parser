package com.android.DOMParser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomListAdapter extends BaseAdapter{
	ArrayList<MyData> allData;
	Activity activity;
	LayoutInflater li;
	ImageDownloader imageDownloader = new ImageDownloader();
	public CustomListAdapter(Activity mainActivity,
			ArrayList<MyData> collection) {
		allData = collection;
		activity = mainActivity;
		li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
	}

	public int getCount() {
		// TODO Auto-generated method stub
		if(allData.size()>0)
		return allData.size();
		else
			return 1;
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		MyData eachMyData;
		class ViewHolder{
			TextView title;
			TextView artist;
			TextView releasedDate;
			TextView price;
			ImageView imageView;
		}
		ViewHolder VH = new ViewHolder();
		if(convertView==null){
			view = li.inflate(R.layout.listlayout, null);
			VH.title = (TextView)view.findViewById(R.id.titleView);
			VH.artist = (TextView)view.findViewById(R.id.artistView);
			VH.releasedDate = (TextView)view.findViewById(R.id.releasedDateView);
			VH.price = (TextView)view.findViewById(R.id.priceView);
			VH.imageView = (ImageView)view.findViewById(R.id.imageView);
			view.setTag(VH);
		}
		else{
			VH = (ViewHolder)view.getTag();
		}
		
	
		eachMyData = (MyData)allData.get(position);
		VH.title.setText(eachMyData.mTitle);
		VH.artist.setText(eachMyData.mArtist);
		
		/*
		 * Date formatting
		 */
		
		
		Date dateFormat = null;
		try {
			dateFormat = new SimpleDateFormat("yyyy-MM-dd")
					.parse(eachMyData.mReleaseDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	
		VH.releasedDate.setText(new SimpleDateFormat("E, MMM d, yyyy").format(dateFormat));
		
		/**                                      **/
		
		
		imageDownloader.download(eachMyData.mImage, VH.imageView);
		VH.price.setText(eachMyData.mPrice);
	
		
			
		
		return view;
	}

}
