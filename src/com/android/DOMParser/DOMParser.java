package com.android.DOMParser;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class DOMParser extends AsyncTask<String, Long, ArrayList<MyData>> {
	private Context ct;
	ProgressDialog dialog;
	ArrayList<MyData> Collection = null;

	public DOMParser(Context context) {
		
		ct = context;
		
	}

	ArrayList<MyData> ParseXML(String urlString) throws ParserConfigurationException,
			SAXException, IOException {
		ArrayList<MyData> collection = new ArrayList<MyData>();
		MyData mydata = new MyData();
		URL url = new URL(urlString);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(new InputSource(url.openStream()));
		doc.getDocumentElement().normalize();
		int j;
		NodeList entries = doc.getElementsByTagName("entry");
		for (int i = 0; i < entries.getLength(); i++) {
			j = 1;
			Element entry = (Element) entries.item(i);
			NodeList children = entry.getChildNodes();
			for (int k = 0; k < children.getLength(); k++) {
				Node child = children.item(k);
				if ((child.getNodeName().equalsIgnoreCase("title"))
						|| (child.getNodeName().equalsIgnoreCase("im:artist"))
						|| (child.getNodeName().equalsIgnoreCase("im:price"))
						|| ((child.getNodeName().equalsIgnoreCase("im:image")) && (((Element) child)
								.getAttribute("height"))
								.equalsIgnoreCase("170"))
						|| (child.getNodeName()
								.equalsIgnoreCase("im:releaseDate"))) {
					Log.d(child.getNodeName(), child.getTextContent());

					switch (j) {
					case 1:
						mydata.mTitle = child.getTextContent();
						break;
					case 2:
						mydata.mArtist = child.getTextContent();
						break;
					case 3:
						mydata.mPrice = child.getTextContent();
						break;
					case 4:
						mydata.mImage = child.getTextContent();
						break;
					case 5:
						mydata.mReleaseDate = child.getTextContent();
						break;
					}
					j++;
					if (j == 6) {
						j = 1;
						collection.add(mydata);
						mydata = new MyData();

					}

				}

			}

		}
		return collection;
	}

	@Override
	protected ArrayList<MyData> doInBackground(String... params) {
		try {

			Collection = ParseXML(params[0]);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Collection;
	}
	@Override
	protected void onPostExecute(ArrayList<MyData> result) {
		WorkDoneListener WDL;
		WDL = (WorkDoneListener) ct;
		 WDL.WorkDone(Collection);
		super.onPostExecute(result);
		

	}

	
}



















