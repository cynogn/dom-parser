package com.android.DOMParser;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

public class ImageDownloader {
	public void download(String url, ImageView imageView) {
		BitmapDownloaderTask task = new BitmapDownloaderTask(imageView);
		DownloadDrawable downloadDrawable = new DownloadDrawable(task);
		imageView.setImageDrawable(downloadDrawable);
		String cookie = null;
		task.execute(url,cookie);
	}
	private static BitmapDownloaderTask getBitmapDownloaderTask(ImageView imageView){
		if(imageView!=null){
			Drawable drawable = imageView.getDrawable();
			if(drawable instanceof DownloadDrawable){
				DownloadDrawable downloadedDrawable = (DownloadDrawable)drawable;
				return downloadedDrawable.getBitmapDownloaderTask();
			}
		}
		return null;
	}
	public class BitmapDownloaderTask extends AsyncTask<String, Void, Bitmap> {
		@SuppressWarnings("unused")
		private String url;
		private final WeakReference<ImageView> imageViewReference;

		public BitmapDownloaderTask(ImageView imageView) {
			imageViewReference = new WeakReference<ImageView>(imageView);
		}

		@Override
		protected Bitmap doInBackground(String... params) {
			// TODO Auto-generated method stub
			return downloadBitmap(params[0]);
		}

		Bitmap downloadBitmap(String urlString) {
			final AndroidHttpClient client = AndroidHttpClient
					.newInstance("Android");
			final HttpGet getRequest = new HttpGet(urlString);

			try {
				HttpResponse response = client.execute(getRequest);
				final int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode != HttpStatus.SC_OK) {
					Log.w("Image Downloader", "Error" + statusCode
							+ "while retreiving image from url" + urlString);
					return null;
				}
				final HttpEntity entity = response.getEntity();
				if (entity != null) {
					InputStream inputStream = null;
					try {
						inputStream = entity.getContent();
						final Bitmap bitmap = BitmapFactory
								.decodeStream(inputStream);
						return bitmap;
					} finally {
						if (inputStream != null) {
							inputStream.close();
						}
						entity.consumeContent();
					}
				}
			} catch (IOException e) {
				getRequest.abort();
				Log.w("ImageDownloader",
						"Error while retrieving Bitmap from url " + urlString);
			} finally {
				if (client != null)
					client.close();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			if(imageViewReference !=null){
				ImageView imageView = imageViewReference.get();
				BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);
				if(this == bitmapDownloaderTask){
					imageView.setImageBitmap(result);
				}
			}
		}

	}
	static class DownloadDrawable extends ColorDrawable{
		private final WeakReference<BitmapDownloaderTask> bitmapDownloaderTaskReference;
		public DownloadDrawable(BitmapDownloaderTask bitMapDownloaderTask){
			super(Color.BLACK);
			bitmapDownloaderTaskReference = new WeakReference<BitmapDownloaderTask>(bitMapDownloaderTask);
		}
		public BitmapDownloaderTask getBitmapDownloaderTask(){
			 return bitmapDownloaderTaskReference.get();
		}
	}
}