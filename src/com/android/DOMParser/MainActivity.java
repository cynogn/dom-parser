package com.android.DOMParser;

import java.util.ArrayList;

import android.R.bool;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

public class MainActivity extends Activity implements WorkDoneListener {
	/** Called when the activity is first created. */
	ProgressDialog pg;
	ListView lv;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		lv = (ListView) findViewById(R.id.listView);

		if (checkForInternet()) {
			pg = new ProgressDialog(this);
			pg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pg.setTitle("Connecting ...");
			pg.setMessage("Please wait while loading data from internet");
			pg.show();
			DOMParser domParser = new DOMParser(this);

			domParser
					.execute("http://itunes.apple.com/us/rss/topalbums/limit=50/xml");
		}
		else{
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setMessage("Could not connect to internet. Check your internet settings")
			.setTitle("No Connection")
			.setPositiveButton("OK", null);
			AlertDialog alertView = alert.create();
			alertView.show();
		
			
		}
	}

	public boolean checkForInternet() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

		return networkInfo != null;
	}

	public void WorkDone(ArrayList<MyData> collection) {
		Log.d("Work Done", "Work is finished");
		pg.dismiss();
		if (collection.size() > 0) {
			CustomListAdapter customListAdapter = new CustomListAdapter(this,
					collection);
			lv.setAdapter(customListAdapter);
		}
		/*
		 * for(MyData list : collection){ Log.d("Title", list.mTitle);
		 * Log.d("Artist",list.mArtist); }
		 */
	}
}